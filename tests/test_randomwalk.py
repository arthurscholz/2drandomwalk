import pytest

import randomwalk

from click.testing import CliRunner
from randomwalk.cli import main
from randomwalk.simulation import Simulation

import numpy as np


def test_main():
    runner = CliRunner()
    result = runner.invoke(main, ['--size', 1, '--model', 'f', '--confidence', .95, '--rel_error', .1])

    assert result.exit_code == 0


def test_size_invalid():
    runner = CliRunner()
    result = runner.invoke(main, ['--size', -1, '--model', 'f', '--confidence', .95, '--rel_error', .1])

    assert result.exit_code == 2


def test_size_valid():
    runner = CliRunner()
    result = runner.invoke(main, ['--size', 1, '--model', 'f', '--confidence', .95, '--rel_error', .1])

    assert result.exit_code == 0


def test_model_invalid():
    runner = CliRunner()
    result = runner.invoke(main, ['--size', 1, '--model', 'q', '--confidence', .95, '--rel_error', .1])

    assert result.exit_code == 2


@pytest.mark.parmetrize("model", ['f', 'a', 'r'])
def test_model_valid():
    runner = CliRunner()
    result = runner.invoke(main, ['--size', 1, '--model', 'f', '--confidence', .95, '--rel_error', .1])

    assert result.exit_code == 0


@pytest.mark.parametrize("size, model, confidence, rel_error", [(1, 'f', .95, .1)])
def test_simulation_init(size, model, confidence, rel_error):
    s = Simulation(size, model, confidence, rel_error)

    assert s.size == size
    assert s.model == model
    assert s.confidence == confidence
    assert s.rel_error == rel_error


def test_west_rule():
    s = Simulation(1, 'f', .95, .1)

    np.testing.assert_array_equal(s.rules[randomwalk.simulation.WEST_RULE_INDEX][randomwalk.simulation.WEST_MOVE_INDEX],
                                  np.array([0, 0, 0]))


def test_east_rule():
    s = Simulation(1, 'f', .95, .1)

    np.testing.assert_array_equal(s.rules[randomwalk.simulation.EAST_RULE_INDEX][randomwalk.simulation.EAST_MOVE_INDEX],
                                  np.array([0, 0, 0]))


def test_south_rule():
    s = Simulation(1, 'f', .95, .1)

    np.testing.assert_array_equal(s.rules[randomwalk.simulation.SOUTH_RULE_INDEX][randomwalk.simulation.SOUTH_MOVE_INDEX],
                                  np.array([0, 0, 0]))


def test_north_rule():
    s = Simulation(1, 'f', .95, .1)

    np.testing.assert_array_equal(s.rules[randomwalk.simulation.NORTH_RULE_INDEX][randomwalk.simulation.NORTH_MOVE_INDEX],
                                  np.array([0, 0, 0]))


def test_sw_rule():
    s = Simulation(1, 'f', .95, .1)

    np.testing.assert_array_equal(s.rules[randomwalk.simulation.SW_RULE_INDEX][randomwalk.simulation.SOUTH_MOVE_INDEX],
                                  np.array([0, 0, 0]))
    np.testing.assert_array_equal(s.rules[randomwalk.simulation.SW_RULE_INDEX][randomwalk.simulation.WEST_MOVE_INDEX],
                                  np.array([0, 0, 0]))


def test_se_rule():
    s = Simulation(1, 'f', .95, .1)

    np.testing.assert_array_equal(s.rules[randomwalk.simulation.SE_RULE_INDEX][randomwalk.simulation.SOUTH_MOVE_INDEX],
                                  np.array([0, 0, 0]))
    np.testing.assert_array_equal(s.rules[randomwalk.simulation.SE_RULE_INDEX][randomwalk.simulation.EAST_MOVE_INDEX],
                                  np.array([0, 0, 0]))


def test_nw_rule():
    s = Simulation(1, 'f', .95, .1)

    np.testing.assert_array_equal(s.rules[randomwalk.simulation.NW_RULE_INDEX][randomwalk.simulation.NORTH_MOVE_INDEX],
                                  np.array([0, 0, 0]))
    np.testing.assert_array_equal(s.rules[randomwalk.simulation.NW_RULE_INDEX][randomwalk.simulation.WEST_MOVE_INDEX],
                                  np.array([0, 0, 0]))
