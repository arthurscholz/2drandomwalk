# Changelog

## 0.1.0 - 2018-11-18
- Added automatic termination conditions for a quasi-relative error

## 0.0.0 - 2018-11-18
- Simulation mechanics only - no automatic termination conditions