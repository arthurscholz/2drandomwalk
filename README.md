
2D Random Walk
==============  
Estimate expected number of time steps for a 2D discrete random walk to reach the termination condition for a specified set of movement models. The three models allowed are: forbidden, absorbed, and reflected. The forbidden model does not allow movement in the direction of a boundary. The absorbed model allows movement but absorbs it (e.g. if a particle moves west on the western boundary is stays put but is penalized a time step). The reflected model allows movement in the direction of a boundary by reflection.  
  
The command to run the simulation is: 
```  
randomwalk --size <N> --model <M> --confidence <C> --relative_error <R>  
```
where:  
```--size``` is the zero-indexed inclusive size of the grid (N corresponds to a grid with (N+1)x(N+1) points)  
```--model``` is ```[f]orbidden```, ```[a]bsorbed```, or ```--[r]eflected```  
```--confidence``` is the desired confidence interval
```--relative_error``` is the termination condition in relative error  
  
Getting Started  
-------------------  
### Prerequisites  
This package was developed and tested with Python 3.7 SciPy 1.1.0 and NumPy 1.15.1. It uses Click 7.0 for the command line interface.  
  
It should work with any version of Python greater than 3.6 and NumPy greater than 1.11.  
  
For testing use PyTest 4.0, PyLint 2.1.1, and Flake8 3.6.0  
  
### Installation  
To install the package run the following command from within your favorite (preferably virtual) Python environment:  
```pip install 2DRandomWalk```  
  
### Tests  
Tests are designed to be run using PyTest, PyLint and Flake8.   
  
#### End-to-end Tests  
To test the package from the root source directory run:  
```pytest```  
  
#### Style Tests  
To validate conformance to PEP-8 (https://www.python.org/dev/peps/pep-0008/) from the root source directory run:  
```flake8 src/```  
```pylint src/```  
  
## Overview  
This code addresses the following problem statement:      
> We are given a two-dimensional (N+1)x(N+1) grid consisting of points (x,y) where x and y are integers ranging from 0 to N (inclusive).
>
> A particle begins at the point (0,0) and at every timestep moves one step on the grid in a random direction (north, south, east, west) chosen uniformly from the alternatives (e.g., from (3,0) it moves north, east, or west with equal probability). > > p(N) is the expected number of timesteps required for a particle starting at (0,0) to reach the point (N,N). >
> 1a) By simulation, estimate p(N) for 1 <= N <= 10. Feel free to use    
any programming language of your choice. 
> 1b) Based on the above estimates, approximately how does p(N) relate    
to N mathematically? 
>
> 2) Answer questions 1a and 1b for a different movement model: now the    
   particle chooses from all four directions uniformly, but if the    
   proposed movement is impossible (e.g., moving south from (3,0)) it    
doesn't move at all for that timestep.
>
> 3) Answer questions 1a and 1b for yet a different movement model; the    
   particle chooses from all four directions uniformly, but if the    
   proposed movement is impossible, it bounces back in the opposite    
   direction (e.g., attempting to move south from (3,0) will move to    
   (3,1)).   
>
> 4) Any additional comments you have on the results are welcome.  
  
The problem statement is asking for a Monte Carlo simulation of the expected value for the number of time steps required to meet the specified termination condition. When considering using a Monte Carlo approach to estimate statistical parameters the two most important considerations are:  
- The quality of the random sequence used to draw from the initial probability distribution  
- Selection of an appropriate estimator and termination conditions  
 
A discussion of the quality of random number sequences can be found in Knuth, D.E., Seminumerical Algorithms, _The Art of Computer Programming_, __Vol. 2__ _3rd. Edition_, Pg. 41-115, (1998). For the purposes for this work we'll assume that the random package from NumPy v. 1.15 is sufficient.

To quote a wise man on the topic of computer simulations: "Never before have we been able to generate so many wrong results so quickly". In other words, we can do a lot of simulations and therefor invoke the Central Limit Theorem.

An unbiased estimator of the variance is provided by:
$S^2_n = \frac{1}{n-1} \sum_{i=1}^{n}(X_i-\bar{X_n})^2$

The absolute error for a specified confidence is:
$Z_c\frac{S_n}{\sqrt{n}} < |\bar{X_n}-\mu|$

It's not quite kosher, but for our purpose we can calculate a relative error by using:
$\frac{|\bar{X}_n-\mu|}{\bar{X}_n}$

instead of the proper:
$\frac{|\bar{X}_n-\mu|}{\mu}$
  
Mechanics  
-------------  
The current state of the simulation is represented at a 3 element vector:  
$<x, y, s>$  
where:  
- $x$ is the horizontal position of the particle  
- $y$ is the vertical position of the particle  
- $s$ is the number of steps  
  
Each movement rule is specified as a 3 element vector:  
$<x, y, s>$  
where:  
- $x$ is the horizontal direction of movement  
- $y$ is the vertical direction of movement  
- $s$ is the step penalty of movement  
  
The movement rules for each grid location is specified by a four element list of vectors:  
[ east rule, west rule, north rule, south rule]  
  
<b>Example: unrestricted movement</b>  
In the absence of boundary conditions the set of movement rules for a grid point is:   
$[ <1, 0, 1>, <-1, 0, 1>, <0, 1, 1>, <0, -1, 1> ]$   
  
<b>Example: forbidden movement</b>  
On a boundary where western movement is forbidden the set of movement rules for a grid point is:   
$[ <1, 0, 1>, <0, 0, 0>, <0, 1, 1>, <0, -1, 1> ] $  
  
<b>Example: absorbed movement</b>  
On a boundary where western movement is absorbed the set of movement rules for a grid point is:   
$[ <1, 0, 1>, <0, 0, 1>, <0, 1, 1>, <0, -1, 1> ]$   
  
<b>Example: reflected movement</b>  
On a boundary where western movement is reflected the set of movement rules for a grid point is:   
$[ <1, 0, 1>, <1, 0, 1>, <0, 1, 1>, <0, -1, 1> ]$  
  
Note: this implementation of mechanics is less efficient for forbidden movement models but simplifies implementation across all three movement rules.  
  
_This document was written to be rendered on stackedit.io_
