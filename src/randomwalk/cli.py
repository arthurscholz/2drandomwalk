"""
Module that contains the command line app.

The command to run the simulation is:
randomwalk --size <N> --model <M> --confidence <C> --relative_error <R>

where:
  --size is the zero-indexed inclusive size of the grid (N corresponds to a
         grid with (N+1)x(N+1) points)
  --model is [f]orbidden, [a]bsorbed, or [r]eflected
  --confidence is the desired confidence interval
  --relative_error is the relative error used for termination
"""

import click
from randomwalk.simulation import Simulation


def validate_model(_, __, value):
    """
    Function DOCSTRING
    :param value:
    :return:
    """
    valid_models = ['f', 'forbidden', 'a', 'absorbing', 'r', 'reflective']
    if value in valid_models:
        return value

    raise click.BadParameter('Invalid model. Choose: (f)orbidden, ' +
                             '(a)bsorbing, or (r)eflective.')


@click.command()
@click.option('--size', prompt="Grid Size: ", type=click.IntRange(1, None),
              help="Zero-indexed inclusive grid size (N corresponds to " +
              "(N+1)x(N+1) points")
@click.option('--model', prompt="Movement Model " +
              "[(f)orbidden, (a)bsorbing, (r)eflective]: ",
              callback=validate_model,
              help="Specify movement model. " +
              "Forbidden does not permit movement against a boundary. " +
              "Absorbing absorbs movements against a boundary. " +
              "Reflecting reflects movements against a boundary.")
@click.option('--confidence', prompt="Confidence interval: ",
              type=click.FloatRange(0, 1),
              help="Desired confidence interval for relative error")
@click.option('--rel_error', prompt="Relative Error: ",
              type=click.FloatRange(0, 1),
              help="Desired relative error for termination")
def main(size, model, confidence, rel_error):
    """
    :param size:
    :param model:
    :param confidence:
    :param rel_error:
    :return:
    """
    simulation = Simulation(size, model[0], confidence, rel_error)
    results = simulation.run()
    print(results.mean())
