"""
File contains the definition of Simulation class which estimates the number
of steps required for a 2D random walk to go from the southwest corner to the
northeast corner of an (N+1)x(N+1) grid.
"""

import numpy as np
import scipy.stats as st

DEFAULT_RULE = [np.array([1, 0, 1]),  # Move east
                np.array([-1, 0, 1]),  # Move west
                np.array([0, 1, 1]),  # Move north
                np.array([0, -1, 1]),  # Move south
                ]

EAST_MOVE_INDEX = 0
WEST_MOVE_INDEX = 1
NORTH_MOVE_INDEX = 2
SOUTH_MOVE_INDEX = 3

# Rule convention:
#  - 0: Default rule (no boundary)
#  - 1: West boundary rule
#  - 2: East boundary rule
#  - 3: North boundary rule
#  - 4: South boundary rule
#  - 5: SW boundary rule
#  - 6: SE boundary rule
#  - 7: NW boundary rule

DEFAULT_RULE_INDEX = 0
WEST_RULE_INDEX = 1
EAST_RULE_INDEX = 2
NORTH_RULE_INDEX = 3
SOUTH_RULE_INDEX = 4
SW_RULE_INDEX = 5
SE_RULE_INDEX = 6
NW_RULE_INDEX = 7


MODELS = {'f': {'reflect': 0, 'penalize': 0},
          'a': {'reflect': 0, 'penalize': 1},
          'r': {'reflect': 1, 'penalize': 1},
          }


class Simulation:
    """
    Simulation class
    """
    def __init__(self, size, model, confidence, rel_error):
        self._size = size
        self._model = model
        self._confidence = confidence
        self._rel_error = rel_error

        self._apply_rules()
        self._create_grid()

    def _apply_rules(self):
        """
        :return:
        """
        self._rules = {}

        reflect = MODELS[self._model]['reflect']
        penalize = MODELS[self._model]['penalize']

        self._rules = dict()

        self._rules[DEFAULT_RULE_INDEX] = DEFAULT_RULE

        west_rule = DEFAULT_RULE.copy()
        west_rule[WEST_MOVE_INDEX] = np.array([reflect, 0, penalize],
                                              dtype=int)
        self._rules[WEST_RULE_INDEX] = west_rule

        east_rule = DEFAULT_RULE.copy()
        east_rule[EAST_MOVE_INDEX] = np.array([-reflect, 0, penalize],
                                              dtype=int)
        self._rules[EAST_RULE_INDEX] = east_rule

        north_rule = DEFAULT_RULE.copy()
        north_rule[NORTH_MOVE_INDEX] = np.array([0, -reflect, penalize],
                                                dtype=int)
        self._rules[NORTH_RULE_INDEX] = north_rule

        south_rule = DEFAULT_RULE.copy()
        south_rule[SOUTH_MOVE_INDEX] = np.array([0, reflect, penalize],
                                                dtype=int)
        self._rules[SOUTH_RULE_INDEX] = south_rule

        sw_rule = DEFAULT_RULE.copy()
        sw_rule[WEST_MOVE_INDEX] = west_rule[WEST_MOVE_INDEX]
        sw_rule[SOUTH_MOVE_INDEX] = south_rule[SOUTH_MOVE_INDEX]
        self._rules[SW_RULE_INDEX] = sw_rule

        se_rule = DEFAULT_RULE.copy()
        se_rule[EAST_MOVE_INDEX] = east_rule[EAST_MOVE_INDEX]
        se_rule[SOUTH_MOVE_INDEX] = south_rule[SOUTH_MOVE_INDEX]
        self._rules[SE_RULE_INDEX] = se_rule

        nw_rule = DEFAULT_RULE.copy()
        nw_rule[WEST_MOVE_INDEX] = west_rule[WEST_MOVE_INDEX]
        nw_rule[NORTH_MOVE_INDEX] = north_rule[NORTH_MOVE_INDEX]
        self._rules[NW_RULE_INDEX] = nw_rule

    def _create_grid(self):
        """
        :return:
        """
        self._grid = np.zeros((self._size+1, self._size+1), dtype=int)

        self._grid[0] = WEST_RULE_INDEX
        self._grid[-1] = EAST_RULE_INDEX
        self._grid[:, 0] = SOUTH_RULE_INDEX
        self._grid[:, -1] = NORTH_RULE_INDEX
        self._grid[0, 0] = SW_RULE_INDEX
        self._grid[-1, 0] = SE_RULE_INDEX
        self._grid[0, -1] = NW_RULE_INDEX

    def walk(self):
        """
        Method DOCSTRING
        :return:
        """
        particle = np.array([0, 0, 0], dtype=int)
        while True:
            rules = self._rules[self._grid[particle[0], particle[1]]]
            move = np.random.randint(0, 4)
            particle += rules[move]

            if particle[0] == self._size and particle[1] == self._size:
                break

        # print('Done!')
        return particle[2]

    def run(self):
        """
        Method DOCSTRING
        :return:
        """
        z_c = st.norm.ppf(self._confidence)

        i = 0
        allocation_size = 100000
        steps = np.zeros(allocation_size)

        while True:
            steps[i] = self.walk()
            i += 1

            if i < 30:
                continue

            abs_error = z_c * steps[:i].std(ddof=1)/np.sqrt(i)

            if abs_error/steps[:i].mean() < self._rel_error:
                break

            if i >= len(steps):
                steps = np.concatenate((steps, np.zeros_like(steps)))

        return steps[:i]

    @property
    def size(self):
        """Property DOCSTRING"""
        return self._size

    @property
    def model(self):
        """Property DOCSTRING"""

        return self._model

    @property
    def confidence(self):
        """Property DOCSTRING"""
        return self._confidence

    @property
    def rel_error(self):
        """Property DOCSTRING"""
        return self._rel_error

    @property
    def rules(self):
        """Property DOCSTRING"""
        return self._rules

    @property
    def grid(self):
        """Property DOCSTRING"""
        return self._grid
